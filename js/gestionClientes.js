var clientesObtenidos;

function getClientes(){
  var url = "https://services.odata.org/v4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if (this.readyState ==4 && this.status == 200){
      // console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("divTablaCli");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for(var i=0; i<JSONProductos.value.length; i++){
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONProductos.value[i].City;

    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    if(JSONClientes.value[i].Contry == "UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    }else{
      imgBandera.src = rutaBandera + JSONClientes.value[i].Contry+".png";
    }

    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTablaCli.appendChild(tabla);
}
